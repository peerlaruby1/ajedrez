﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ProyectoAjedrez;
using LogicaNegocios.ProyectoAjedrez;


namespace ProyectoAjedrez
{
    public partial class fk_hotel : Form
    {
        DataSet ds = new DataSet();
        private HotelManejador _hotelManejador;
        public fk_hotel()
        {
            InitializeComponent();
            _hotelManejador = new HotelManejador();
        }
        private void BuscarHotel(string filtro)
        {
            dgvHotel.DataSource = _hotelManejador.GetHotel(filtro);
        }
        private void GuardarHotel()
        {
            _hotelManejador.Guardar(new Hotel
            {
                Id_hotel = Convert.ToInt32(lbl_id.Text),
                Nombre_hotel = txt_nombre.Text,
                Direcion_hotel = txt_direccion.Text,
                Telefono_hotel = txt_telefono.Text
            });
        }

        private void ModificarHotel()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lbl_id.Text = dgvHotel.CurrentRow.Cells["id_hotel"].Value.ToString();
            //txt_folio.Text = dgvHotel.CurrentRow.Cells["id_hotel"].Value.ToString();
            txt_nombre.Text = dgvHotel.CurrentRow.Cells["nombre_hotel"].Value.ToString();
            txt_direccion.Text = dgvHotel.CurrentRow.Cells["direcion_hotel"].Value.ToString();
            txt_telefono.Text = dgvHotel.CurrentRow.Cells["telefono_hotel"].Value.ToString();
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            Btn_nuevo.Enabled = Nuevo;
            Btn_guardar.Enabled = Guardar;
            Btn_cancelar.Enabled = Cancelar;
            Btn_eliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txt_nombre.Enabled = activar;
            txt_direccion.Enabled = activar;
            txt_folio.Enabled = activar;
            txt_telefono.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txt_nombre.Text = "";
            txt_folio.Text = "";
            txt_telefono.Text = "";
            txt_direccion.Text = "";
            txt_buscar.Text = "";
        }
        private void EliminarHotel()//necesitamos el id int para que elimine el dato
        {
            var IdHotel = dgvHotel.CurrentRow.Cells["id_hotel"].Value;//DEPENDE DEL DQATA
            _hotelManejador.Eliminar(Convert.ToInt32(IdHotel));//EL METODO LO NECESITA INT
        }



        private void fk_hotel_Load(object sender, EventArgs e)
        {
            BuscarHotel("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            txt_buscar.Text ="";

        }

        private void gpoHotel_Enter(object sender, EventArgs e)
        {

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, true, true, false);
            ControlarCuadros(true);
            gpoHotel.Visible = true;
            dgvHotel.Visible = false;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            gpoHotel.Visible = false;
            dgvHotel.Visible = true;
        }

        private void dgvHotel_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                gpoHotel.Visible = true;
                dgvHotel.Visible = false;
                ModificarHotel();

                BuscarHotel("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lbl_buscar_TextChanged(object sender, EventArgs e)
        {
            BuscarHotel(txt_buscar.Text);
        }

        private void Btn_guardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarHotel();
                LimpiarCuadros();
                BuscarHotel("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            gpoHotel.Visible = false;
            dgvHotel.Visible = true;
        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarHotel();
                    BuscarHotel("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
                gpoHotel.Visible = false;
                dgvHotel.Visible = true;
            }
        }
    }
}