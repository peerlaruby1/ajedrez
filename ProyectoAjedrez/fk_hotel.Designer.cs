﻿namespace ProyectoAjedrez
{
    partial class fk_hotel
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fk_hotel));
            this.txt_folio = new System.Windows.Forms.MaskedTextBox();
            this.txt_nombre = new System.Windows.Forms.MaskedTextBox();
            this.txt_direccion = new System.Windows.Forms.MaskedTextBox();
            this.txt_telefono = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvHotel = new System.Windows.Forms.DataGridView();
            this.txt_buscar = new System.Windows.Forms.MaskedTextBox();
            this.lbl_buscar = new System.Windows.Forms.Label();
            this.lbl_id = new System.Windows.Forms.Label();
            this.gpoHotel = new System.Windows.Forms.GroupBox();
            this.Btn_eliminar = new System.Windows.Forms.Button();
            this.Btn_cancelar = new System.Windows.Forms.Button();
            this.Btn_guardar = new System.Windows.Forms.Button();
            this.Btn_nuevo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHotel)).BeginInit();
            this.gpoHotel.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_folio
            // 
            this.txt_folio.Location = new System.Drawing.Point(5, 30);
            this.txt_folio.Name = "txt_folio";
            this.txt_folio.Size = new System.Drawing.Size(71, 20);
            this.txt_folio.TabIndex = 0;
            // 
            // txt_nombre
            // 
            this.txt_nombre.Location = new System.Drawing.Point(5, 69);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(212, 20);
            this.txt_nombre.TabIndex = 1;
            // 
            // txt_direccion
            // 
            this.txt_direccion.Location = new System.Drawing.Point(5, 108);
            this.txt_direccion.Name = "txt_direccion";
            this.txt_direccion.Size = new System.Drawing.Size(212, 20);
            this.txt_direccion.TabIndex = 2;
            // 
            // txt_telefono
            // 
            this.txt_telefono.Location = new System.Drawing.Point(5, 147);
            this.txt_telefono.Name = "txt_telefono";
            this.txt_telefono.Size = new System.Drawing.Size(212, 20);
            this.txt_telefono.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Folio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nombre ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Direccion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Telefono";
            // 
            // dgvHotel
            // 
            this.dgvHotel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHotel.Location = new System.Drawing.Point(18, 54);
            this.dgvHotel.Name = "dgvHotel";
            this.dgvHotel.Size = new System.Drawing.Size(491, 256);
            this.dgvHotel.TabIndex = 8;
            this.dgvHotel.DoubleClick += new System.EventHandler(this.dgvHotel_DoubleClick);
            // 
            // txt_buscar
            // 
            this.txt_buscar.Location = new System.Drawing.Point(85, 12);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(316, 20);
            this.txt_buscar.TabIndex = 9;
            this.txt_buscar.TextChanged += new System.EventHandler(this.lbl_buscar_TextChanged);
            // 
            // lbl_buscar
            // 
            this.lbl_buscar.AutoSize = true;
            this.lbl_buscar.Location = new System.Drawing.Point(39, 15);
            this.lbl_buscar.Name = "lbl_buscar";
            this.lbl_buscar.Size = new System.Drawing.Size(40, 13);
            this.lbl_buscar.TabIndex = 10;
            this.lbl_buscar.Text = "Buscar";
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Location = new System.Drawing.Point(496, 15);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(13, 13);
            this.lbl_id.TabIndex = 11;
            this.lbl_id.Text = "0";
            this.lbl_id.Visible = false;
            // 
            // gpoHotel
            // 
            this.gpoHotel.Controls.Add(this.label4);
            this.gpoHotel.Controls.Add(this.label3);
            this.gpoHotel.Controls.Add(this.label2);
            this.gpoHotel.Controls.Add(this.label1);
            this.gpoHotel.Controls.Add(this.txt_telefono);
            this.gpoHotel.Controls.Add(this.txt_direccion);
            this.gpoHotel.Controls.Add(this.txt_nombre);
            this.gpoHotel.Controls.Add(this.txt_folio);
            this.gpoHotel.Location = new System.Drawing.Point(18, 54);
            this.gpoHotel.Name = "gpoHotel";
            this.gpoHotel.Size = new System.Drawing.Size(491, 256);
            this.gpoHotel.TabIndex = 16;
            this.gpoHotel.TabStop = false;
            this.gpoHotel.Visible = false;
            this.gpoHotel.Enter += new System.EventHandler(this.gpoHotel_Enter);
            // 
            // Btn_eliminar
            // 
            this.Btn_eliminar.BackColor = System.Drawing.Color.Transparent;
            this.Btn_eliminar.BackgroundImage = global::ProyectoAjedrez.Properties.Resources.icons8_trash_can_480;
            this.Btn_eliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_eliminar.Location = new System.Drawing.Point(455, 314);
            this.Btn_eliminar.Name = "Btn_eliminar";
            this.Btn_eliminar.Size = new System.Drawing.Size(54, 55);
            this.Btn_eliminar.TabIndex = 15;
            this.Btn_eliminar.UseVisualStyleBackColor = false;
            this.Btn_eliminar.Click += new System.EventHandler(this.Btn_eliminar_Click);
            // 
            // Btn_cancelar
            // 
            this.Btn_cancelar.BackColor = System.Drawing.Color.Transparent;
            this.Btn_cancelar.BackgroundImage = global::ProyectoAjedrez.Properties.Resources.icons8_cancel_480;
            this.Btn_cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_cancelar.Location = new System.Drawing.Point(395, 314);
            this.Btn_cancelar.Name = "Btn_cancelar";
            this.Btn_cancelar.Size = new System.Drawing.Size(54, 55);
            this.Btn_cancelar.TabIndex = 14;
            this.Btn_cancelar.UseVisualStyleBackColor = false;
            this.Btn_cancelar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // Btn_guardar
            // 
            this.Btn_guardar.BackColor = System.Drawing.Color.Transparent;
            this.Btn_guardar.BackgroundImage = global::ProyectoAjedrez.Properties.Resources.icons8_save_80;
            this.Btn_guardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_guardar.Location = new System.Drawing.Point(335, 314);
            this.Btn_guardar.Name = "Btn_guardar";
            this.Btn_guardar.Size = new System.Drawing.Size(54, 55);
            this.Btn_guardar.TabIndex = 13;
            this.Btn_guardar.UseVisualStyleBackColor = false;
            this.Btn_guardar.Click += new System.EventHandler(this.Btn_guardar_Click);
            // 
            // Btn_nuevo
            // 
            this.Btn_nuevo.BackColor = System.Drawing.Color.Transparent;
            this.Btn_nuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_nuevo.BackgroundImage")));
            this.Btn_nuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Btn_nuevo.Location = new System.Drawing.Point(275, 316);
            this.Btn_nuevo.Name = "Btn_nuevo";
            this.Btn_nuevo.Size = new System.Drawing.Size(54, 53);
            this.Btn_nuevo.TabIndex = 12;
            this.Btn_nuevo.UseVisualStyleBackColor = false;
            this.Btn_nuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // fk_hotel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(533, 374);
            this.Controls.Add(this.gpoHotel);
            this.Controls.Add(this.Btn_eliminar);
            this.Controls.Add(this.Btn_cancelar);
            this.Controls.Add(this.Btn_guardar);
            this.Controls.Add(this.Btn_nuevo);
            this.Controls.Add(this.lbl_id);
            this.Controls.Add(this.lbl_buscar);
            this.Controls.Add(this.txt_buscar);
            this.Controls.Add(this.dgvHotel);
            this.Name = "fk_hotel";
            this.Text = "Registro de hotel";
            this.Load += new System.EventHandler(this.fk_hotel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHotel)).EndInit();
            this.gpoHotel.ResumeLayout(false);
            this.gpoHotel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox txt_folio;
        private System.Windows.Forms.MaskedTextBox txt_nombre;
        private System.Windows.Forms.MaskedTextBox txt_direccion;
        private System.Windows.Forms.MaskedTextBox txt_telefono;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvHotel;
        private System.Windows.Forms.MaskedTextBox txt_buscar;
        private System.Windows.Forms.Label lbl_buscar;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Button Btn_nuevo;
        private System.Windows.Forms.Button Btn_guardar;
        private System.Windows.Forms.Button Btn_cancelar;
        private System.Windows.Forms.Button Btn_eliminar;
        private System.Windows.Forms.GroupBox gpoHotel;
    }
}

