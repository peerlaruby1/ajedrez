﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedrez;
using AccesoDatos.ProyectoAjedrez;

namespace LogicaNegocios.ProyectoAjedrez
{
    public class HotelManejador
    {
        private HotelAccesoDatos _hotelAccesoDatos = new HotelAccesoDatos();
        public void Guardar(Hotel hotel)
        {
            _hotelAccesoDatos.Guardar(hotel);
        }
        public void Eliminar(int idHotel)
        {
            _hotelAccesoDatos.Eliminar(idHotel);
        }
        public List<Hotel> GetHotel(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();

            var listHotel = _hotelAccesoDatos.GetHotel(filtro);

            //Llenar lista
            return listHotel;
        }
    }
}
