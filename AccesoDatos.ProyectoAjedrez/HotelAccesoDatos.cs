﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ProyectoAjedrez;
using System.Data;
using Entidades.ProyectoAjedrez;
using System.Data;

namespace AccesoDatos.ProyectoAjedrez
{
    public class HotelAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public HotelAccesoDatos()
            {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Ajedrez", 3306);
            }

        public void Guardar(Hotel hotel)
        {
            if (hotel.Id_hotel == 0)
            {
               
                //Insertar
                string consulta = string.Format("call todohotel3(null,'{0}','{1}','{2}' ",/* hotel.Id_hotel,*/ hotel.Nombre_hotel,hotel.Direcion_hotel,hotel.Telefono_hotel);
                conexion.EjecutarConsulta(consulta);
            }
            else 
            {
                //update
                string consulta = string.Format("call modificarhotel('{0}','{1}','{2}','{3}')", hotel.Nombre_hotel,hotel.Direcion_hotel,hotel.Telefono_hotel,hotel.Id_hotel);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idHotel)
        {
            //Eliminar
            string consulta = string.Format("call eliminarhotel {0}", idHotel);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Hotel> GetHotel(string filtro)
        {
            var Listhotel = new List<Hotel>();// SE ALAMCENARA EN ESTE USUARIO 
            var ds = new DataSet();
            string consulta = "Call hotelmostrar ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "Hotel");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var hotel = new Hotel
                {
                    Id_hotel = Convert.ToInt32(row["id_hotel"]),
                    Nombre_hotel = row["nombre_hotel"].ToString(),
                    Direcion_hotel = row["direccion_hotel"].ToString(),
                    Telefono_hotel = row["telefono_hotel"].ToString(),

                };
                Listhotel.Add(hotel);
            }
            return Listhotel;
        }
    }
}
