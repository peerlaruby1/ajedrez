﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ProyectoAjedrez
{
    public class Hotel
    {
        private int id_hotel;
        private string nombre_hotel;
        private string direcion_hotel;
        private string telefono_hotel;


        public int Id_hotel { get => id_hotel; set => id_hotel = value; }
        public string Nombre_hotel { get => nombre_hotel; set => nombre_hotel = value; }
        public string Direcion_hotel { get => direcion_hotel; set => direcion_hotel = value; }
        public string Telefono_hotel { get => telefono_hotel; set => telefono_hotel = value; }
    }
}
